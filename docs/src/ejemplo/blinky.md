# blinky

```Python
import control_host as ch
import asyncio as aio

###### Config ######

schema = ch.image('../firmware/target/thumbv7em-none-eabihf/release/sc-cfe')
port = ch.open_serial(schema, "/dev/ttyACM0", 192000)

###### Programa ######

async def main():
    global port
    while (true):
        await port.led.blue()
        await aio.sleep(1)
        await port.led.off()
        await aio.sleep(1)

aio.run(main())
```
