# Comandos

La sintaxis de los comandos es:

```Python
port.instrumento.comando(param1 = val1, .... paln = n)
```

Para este montaje experimental se dispone de los siguientes intrumentos con los siguientes comandos:

## `led`

- `red()`: hace que el led del embebido muestre color rojo
- `green()`: hace que el led del embebido muestre color verde
- `blue()`: hace que el led del embebido muestre color azul
- `off()`:  apaga el led del embebido

## `termocupla`

- `temp()`: pide una muestra de temperatura (cC°)

## `mks979b` (Medidor de presión conectado a la unidad PDR 900)

- `pressure()`: retorna la medida de presión en TORR (retorna SciNumber o None)

## `pr4000b` (Controlador de flujo de masas)

- `flow_ch1()`: Obtiene el valor actual de flujo del canal 1 en SCCM (retorna FlowValue o None)
- `flow_ch2()`: Obtiene el valor actual de flujo del canal 2 en SCCM (retorna FlowValue o None)
- `setpoint_ch1(setpoint = [FlowValue]):` Configura el valor del setpoint del canal 1.
- `setpoint_ch2(setpoint = [FlowValue]):` Configura el valor del setpoint del canal 2.
- `valve_ch1(enable = [True | False] )`: Retorna `True` si se pudo ejecutar el comando
    - enable = True: Habilita el canal para inyección 
    - enable = False:  Deshabilita el canal para la inyección
- `valve_ch2(enable = [True | False])`: Retornar `True` si se pudo ejecutar el comando
    - `enable = True`: Habilita el canal para inyección 
    - `enable = False`:  Deshabilita el canal para la inyección
- `gas_injection(turn_on = [True | False])`: 
    - `turn_on = True`: Habilita la inyección de gases
    - `turn_off = False`: Deshabilita inyección de gases


# Tipos de datos adicionales 

Para tipos de datos complejos, se tienen objetos por aparte cuyas propiedades se pueden acceder como `valor.propiedad`

## FlowValue 

- `value`: valor sin procesar, unidades SCCM 
- `decimal_point`: posición del punto decimal

Es decir, si tenemos que 

```Python
x =  await port.pr4000b.flow_ch1() 
if x != None: 
    flujo = x.value / (10**x.decimal_point)
```

Osea flujo = x * 10e-(decimal_point)

##  SciNumber

- `integer`: Valor de parte entera del numero
- `decimal`: Valor de la parte decimal
- `epositive`: indica si el exponente es positivo
- `exponent`: valor del exponente

Osea, el numero es `[integer].[decimal]E[sign][exponent]`

## Funciones de utilidad

```Python
def sci_a_str(x):
     sign = "+" if x.epositive else "-"
     return "{}.{:02d}E{}{}".format(x.integer, x.decimal, sign, x.exponent)

def sci_a_float(x):
     return float(sci_a_str(x))

def flowvalue_a_float(x):
     return x.value / (10**x.decimal_point)
```
