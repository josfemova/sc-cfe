# Introduccion

## Equipo

En el laboratorio ya hay una laptop con lo necesario para utilizar el sistema. Contacte con José Morales para coodinar uso guiado del equipo:

Telegram: [`@josfemova`](https://t.me/josfemova)

## Repositorio con archivos

El repositorio con esta documentación y los demás archivos necesarios para utiliza el sistema se encuentra en:

<https://gitlab.com/josfemova/sc-cfe>

La estructura del directorio es la siguiente

- `docs`: archivos que generan esta documentación

- `ejemplos`: Archivos de python con ejemplos de como se usa la biblioteca, más la biblioteca de python compilada para linux

- `firmware`: Proyecto de Rust con el firmware del microcontrolador

## Requisitos para uso en equipo propio

1. Instalar Rust
   - <https://www.rust-lang.org/tools/install>

2. Instalar toolchain de nightly

Abrir una consola de comandos:

```Shell
rustup default nightly
```

3. Instalar lo necesario para crosscompilar para el sistema embebido

```Shell
rustup target install thumbv7em-none-eabihf
```

4. Instalar herramienta para cargar programas al embebido

```Shell
cargo install cargo-hf2
```
5. Biblioteca de pyserial

```Shell
pip install pyserial
```

## Uso básico

Cómo mínimo, se requiere de las bibliotecas `asyncio` y la biblioteca de `control_host` cuyo archivo debe estar en la misma carpeta que se ejecuta el archivo de python. 

```Python
import control_host as ch
import asyncio as aio
```

En primer lugar se debe cargar el schema que describe el sistema. Esto
se hace llamando a la función "image" de la biblioteca `control_host`

```Python
schema = ch.image('archivo-de-firmware')
```

El archivo del firmware en el caso de el repositorio que van a usar sería entonces

```Python
schema = ch.image('../firmware/target/thumbv7em-none-eabihf/release/sc-cfe')
```

Y luego debemos obtener el puerto serial para comunicarnos con el embebido:

```Python
port = ch.open_serial(schema, "/dev/ttyACM0", 192000)
```

El objeto `port` se usará para interactuar con el sistema.


## Encontrar puerto serial

Usando pyserial podemos encontrar el puerto serial.
En la programación del microcontrolador se ha modificado el nombre
de manufacturador y de producto para hacer de esto un proceso más simple:

```Python
# Encontrar microcontroladores de PlasmaTEC
from serial.tools.list_ports import comports, ListPortInfo

puertos = comports()
mi_puerto = None

for puerto in puertos:
    if puerto.manufacturer == "PlasmaTEC" and puerto.product == "Control_Camara":
        mi_puerto = puerto.device

if mi_puerto == None:
    exit()

print(mi_puerto)
```

## Autoría

Los componentes necesarios para el sistema fueron desarrollados por Alejandro Soto Chacón y José Fernando Morales Vargas. 
