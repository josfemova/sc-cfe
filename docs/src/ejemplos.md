# Ejemplos

En el repositorio se adjuntan algunos ejemplos. Para probarlos primero se debe construír el firmware del dispositivo.
Para esto tiene que:

1. Conectar el microcontrolador a la computadora
2. Presionar dos veces el botón del microcontrolador para ponerlo en modo de programación
3. Ejecutar en una consola dentro de la carpeta firmware `cargo hf2 --release`

Esto debería producir el archivo de firmware que luego se utiliza como schema.

### `blinky`

Prende y apaga el led del sistema en intervalos de 1 segundo.

### `presion_temp`

Este ejemplo consiste de un programa que trata de obtener un valor de presión y otro de temperatura.
Termina al obtener ambos.

### `temp_log_consola`

Este ejemplo consiste en un programa que loggea constantemente valores de presión
y temperatura, pero en otro hilo corre una aplicacion de consola la cual permite dos comandos:

- stop: detiene el loggeo de dato y el programa
- print: imprime los valores actuales de presion y temperatura

### `tkinter_matplotlib_temp`

Aplicación GUI usando tkinter. Contiene un gráfico de matplotlib 
en el que se puede visualizar la temperatura actual.

### `sistema_completo`

Este ejemplo presenta un sistema que permite
- Loggear datos de presion, temperatura y flujo de gas
- Visualizar los datos de presion, temperatura y flujo de gas en gráficas de matplotlib
- Controlar la inyección de gases
