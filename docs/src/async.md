# Biblioteca asyncio

La biblioteca `control_host` está diseñada para programación ***asincrónica***

En resumidas cuentas, esto significa que se pueden ejecutar varios comandos al mismo tiempo sin necesidad de esperar a que otro comando reciba una respuesta. 

La siguiente explicación es un poco simplificada, por lo que sería bueno que si tienen dudas lean un poco más sobre el tema. La documentación de las funciones usadas se encuentra en <https://docs.python.org/3/library/asyncio-task.html>

## Un hola mundo 

```Python
import asyncio as aio
```

Noten que la función se anota con `async`

```Python
async def main():
    print("hola")
    # pausa sin bloquear ejecución de otros comandos
    await aio.sleep(1)
    print("mundo")
```

Para ejecutar nuestro main `async` usamos la función `run` de `asyncio` con nuestra función `async` de argumento.

```Python
aio.run(main())
```
En consola veremos:

```Shell
hola mundo
```

Las funciones async solo van a ejecutarse con ciertos métodos de `asyncio` y dentro de otras funciones `async` usando la operación de `await`

```Python
import asyncio as aio

async def funA():
   print("world")

async def funB():
   print("hello")
   await funA()

aio.run(funB())
```

`await` impedirá que la función asincrónica actual continúe con la siguiente línea hasta que la función a la que se está esperando complete su trabajo. Para ejecutar varios comandos simultáneamente entonces se requiere de concurrencia.

## Concurrencia

Para correr varios comandos al mismo tiempo se usa la función `gather`:

```Python
import asyncio as aio
import randint from random

async def A():
   await aio.sleep(randint(0,6))
   print("A")

async def B():
   await aio.sleep(randint(0,6))
   print("B")

async def C():
   await aio.sleep(randint(0,6))
   print("C")

async def main():
    await aio.gather(
        A(),
        B(),
        C()
    )
    print("fin")

aio.run(main())
```

- El programa anterior tratará de completar A, B y C de forma concurrente. Cuando las 3 tareas hayan terminado, imprimirá "fin".

- Como los tiempos de espera se asignan al azar, B puede terminar antes que A, C antes que B, etc etc. Es decir, el orden de completitud puede ser cualquier, pero sí esperará a que las 3 tareas se completen. 

- Para contextualizar las cosas, los comandos de `control_host` son funciones async tal como A, B y C

Con esto se completarían los bloques de construcción para aplicaciones más complejas. Los datos pueden graficarlos con bibliotecas como matplotlib, pueden hacer aplicaciones gráficas más elaboradas, pueden guardar los datos en archivos csv que luego usarán para analizar la práctica de laboratorio, etc.

