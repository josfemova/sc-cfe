"""
Descripción del ejemplo:

Este ejemplo consiste en un programa que loggea constantemente valores de presión
y temperatura, pero en otro hilo corre una aplicacion de consola la cual permite dos comandos:

- stop: detiene el loggeo de dato y el programa
- print: imprime los valores actuales de presion y temperatura
"""

import control_host as ch
import asyncio as aio
# para usar como cronómetro
from time import perf_counter_ns
from datetime import datetime
# Para el programa de consola
from threading import Thread
import signal

###### Config ######

from serial.tools.list_ports import comports

puertos = comports()
mi_puerto = None

for puerto in puertos:
    if puerto.manufacturer == "PlasmaTEC" and puerto.product == "Control_Camara":
        mi_puerto = puerto.device

if mi_puerto == None:
    print("No se logró encontrar el microcontrolador")
    exit(1)

schema = ch.image('../firmware/target/thumbv7em-none-eabihf/release/sc-cfe')
port = ch.open_serial(schema, mi_puerto, 192000)

###### Utilidades ######

def sci_a_str(x):
     sign = "+" if x.epositive else "-"
     return "{}.{:02d}E{}{}".format(x.integer, x.decimal, sign, x.exponent)

###### Programa ######
presion = 0.0
temperatura = 0.0

async def obtener_presion():
    global port, presion 
    x = await port.mks979b.pressure()
    if x!= None:
        presion = sci_a_str(x)

async def obtener_temp():
    global port, temperatura
    temp = await port.termocupla.temp()


RUNNING = True
def signal_handler(sig, frame):
    global RUNNING
    RUNNING = False

# en vez de cerrar el programa con ctrl C, cambiamos el valor de la global RUNNING
signal.signal(signal.SIGINT, signal_handler)

async def main():
    global port, RUNNING
    filename = "./datos_{}.csv".format(datetime.now())
    archivo = open(filename, "w")
    archivo.write("Tiempo en ns, Temperatura °C,Presion Torr\n")
    await port.led.green()
    while RUNNING:
        await aio.gather(
                obtener_temp(),
                obtener_presion())

        archivo.write("{},{},{}\n".format(
                perf_counter_ns(),
                temperatura, 
                presion))
    archivo.close()
    await port.led.red()

# lanzamos el programa de async io en otro thread
hilo = Thread(target = lambda: aio.run(main()))
hilo.start()

def programa_consola():
    global RUNNING, presion, temperatura
    while RUNNING:
        comando = input("Siguiente comando?\n ")
        if comando == "stop":
            RUNNING = False
            print("fin del programa")
        if comando == "print":
            print("presion :{} -- temp:{}".format(presion, temperatura))


programa_consola()




