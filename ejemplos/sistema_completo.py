"""
Descripción del ejemplo:

Este ejemplo presenta un sistema que permite
- Loggear datos de presion, temperatura y flujo de gas
- Visualizar los datos de presion, temperatura y flujo de gas en gráficas de matplotlib
- Controlar la inyección de gases
"""

import control_host as ch
import asyncio as aio
from tkinter import *
from matplotlib.figure import Figure
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from threading import Thread

from datetime import datetime
from time import perf_counter_ns
from time import sleep


#### Para generar varios gráficos sin problemas
class grafico:
    def __init__(self, root,poll_val,coords, x = [], y= [], size = (5,3), ylim = (10,100), wait_time=100, semilogy = False):
        self.x = x
        self.y = y
        self.resetx = x
        self.resety = y
        self.i = 0
        self.fig = Figure(figsize=size, dpi = 100)
        self.ax = self.fig.add_subplot()
        if semilogy:
            self.line, = self.ax.semilogy(x,y)
        else :
            self.line, = self.ax.plot(x,y)
        self.ax.set_ylim(ylim)

        # canvas de tkinter
        self.canvas = FigureCanvasTkAgg(self.fig, master = root)
        self.canvas.draw()
        self.animation = FuncAnimation(self.fig, self.update, repeat = False,blit=False, interval=wait_time)
        self.poll_val = poll_val
        self.root = root
        self.canvas.get_tk_widget().place(x=coords[0], y=coords[1])
        self.activo = True
    
    def update(self,frame):
        self.x.append(self.i)
        self.i = self.i+1
        self.y.append(self.poll_val())
        self.x = self.x[-200:]
        self.y = self.y[-200:]
        self.line.set_data(self.x, self.y)
        self.ax.relim()
        self.ax.autoscale_view(True)
        return self.line,

    def toggle(self):
        if self.activo:
            self.animation.pause()
            self.x = self.resetx
            self.y = self.resety
            self.activo = False
        else:
            self.activo = True
            self.animation.resume()



# ventana de tkinter
root = Tk()
root.minsize(1400,800)
root.resizable(width=NO, height=NO)
pantalla = Canvas(root, width=1800, height=900, bg='white')
pantalla.place(x=0,y=0)

ui_font = ("Hack", 19)

temp_label = pantalla.create_text(200, 500, font = ui_font)
presion_label = pantalla.create_text(200, 550, font = ui_font)
flujo1_label = pantalla.create_text(200, 600, font = ui_font)
flujo2_label = pantalla.create_text(200, 650, font = ui_font)
inyeccion_label = pantalla.create_text(200, 700, font = ui_font)

pantalla.itemconfigure(flujo1_label, text = "Flujo CH1 _.__ SCCM")
pantalla.itemconfigure(flujo2_label, text = "Flujo CH2 _.__ SCCM")
pantalla.itemconfigure(presion_label, text = "Presión: _.__E+__  Torr")
pantalla.itemconfigure(temp_label, text = "Temperatura: _ °C")
pantalla.itemconfigure(inyeccion_label, text = "NO SE ESTÁ INYECTANDO GAS")

###### Config ######

from serial.tools.list_ports import comports

puertos = comports()
mi_puerto = None

for puerto in puertos:
    if puerto.manufacturer == "PlasmaTEC" and puerto.product == "Control_Camara":
        mi_puerto = puerto.device

if mi_puerto == None:
    print("No se logró encontrar el microcontrolador")
    exit(1)

schema = ch.image('../firmware/target/thumbv7em-none-eabihf/release/sc-cfe')
port = ch.open_serial(schema, mi_puerto, 192000)

###### Utilidades ######

def sci_a_str(x):
     sign = "+" if x.epositive else "-"
     return "{}.{:02d}E{}{}".format(x.integer, x.decimal, sign, x.exponent)

def flowvalue_a_float(x):
     return x.value / (10**x.decimal_point)

###### Programa ######

temperatura = 0.0
presion = 0.0
flujo1 = 0.0
flujo2 = 0.0
RUNNING = True
LOGGING = True


async def t_temperatura():
    global port, pantalla ,temperatura, temp_label
    try:
        var = await port.termocupla.temp()
        temperatura = var #/10.0
        pantalla.itemconfigure(temp_label, text = "Temperatura: {} °C".format(temperatura))
    except: 
        print("temperatura desconectada")
        await aio.sleep(4)

async def t_presion():
    global port, pantalla, presion, presion_label
    try:
        x = await port.mks979b.pressure()
        if x!= None:
            presion_str = sci_a_str(x)
            presion = float(presion_str) #para grafica se necesita float
            pantalla.itemconfigure(presion_label, text = "Presión: {} Torr".format(presion_str))
    except:
        print("presion desconectada")
        await aio.sleep(4)

# Para comandos que queremos ejecutar una sola vez
MISC_TASKS = []
async def t_flujo1():
    global port,pantalla, flujo1, flujo1_label,flujo2, flujo2_label, MISC_TASKS 
    try:
        x = await port.pr4000b.flow_ch1()
        if x!= None:
            flujo1 = flowvalue_a_float(x)
            pantalla.itemconfigure(flujo1_label, text = "Flujo CH1 {} SCCM".format(flujo1))
    except: 
        print("flujo desconectado")
        await aio.sleep(4)
    try:
        x = await port.pr4000b.flow_ch2()
        if x!= None:
            flujo2 = flowvalue_a_float(x)
            pantalla.itemconfigure(flujo2_label, text = "Flujo CH2 {} SCCM".format(flujo2))
    except: 
        print("flujo desconectado")
        await aio.sleep(4)
    for i in MISC_TASKS:
        # si hay una tarea pendiente, la ejecuta
        # y la elimina de la cola de tareas
        await MISC_TASKS.pop()()



async def encender_inyeccion():
    global port,btn_log_on, btn_log_off
    try:
        # Asegurarse de que valvula 2 está activada
        exito = await port.pr4000b.valve_ch2(enable = True)
        if (exito != True):
            raise SystemError("Comando no ejecutado")
        
        # Asegurarse de que valvula 1 está activada
        exito = await port.pr4000b.valve_ch1(enable = True)
        if (exito != True):
            raise SystemError("Comando no ejecutado")

        # Activar inyección de gases
        exito = await port.pr4000b.gas_injection(turn_on = True)
        if exito:
            btn_inyeccion_off["state"] = "normal"
            btn_inyeccion_on["state"] = "disabled"
            pantalla.itemconfigure(inyeccion_label, text = "SE ESTÁ INYECTANDO GAS")
        else:
            # trata de apagar la inyección de gases si algo salió mal
            await wait_timeout(port.pr4000b.gas_injection(turn_on = False))
            pantalla.itemconfigure(inyeccion_label, text = "Algo falló al iniciar inyección de gas")
    except Exception as e:
        print("No hay comunicación con el sistema de inyección de flujo")

async def config_primera_inyeccion():
    val_ch1 = schema.FlowValue(value = 0000, decimal_point = 2)
    val_ch2 = schema.FlowValue(value = 2000, decimal_point = 2)
    #val_ch1 = schema.FlowValue(value = 500, decimal_point = 2)
    #val_ch2 = schema.FlowValue(value = 1500, decimal_point = 2)
    try:
        exito = await port.pr4000b.setpoint_ch1(setpoint = val_ch1)
        exito = await port.pr4000b.setpoint_ch2(setpoint = val_ch2)
    except:
        print("No hay comunicación con el sistema de inyección de flujo")

async def config_segunda_inyeccion():
    val_ch1 = schema.FlowValue(value = 500, decimal_point = 2)
    val_ch2 = schema.FlowValue(value = 1500, decimal_point = 2)
    #val_ch1 = schema.FlowValue(value = 1000, decimal_point = 2)
    #val_ch2 = schema.FlowValue(value = 1000, decimal_point = 2)
    try:
        exito = await port.pr4000b.setpoint_ch1(setpoint = val_ch1)
        exito = await port.pr4000b.setpoint_ch2(setpoint = val_ch2)
    except:
        print("No hay comunicación con el sistema de inyección de flujo")

async def apagar_inyeccion():
    global port,btn_log_on, btn_log_off
    try:
        exito = await port.pr4000b.gas_injection(turn_on= False)
        if exito:
            btn_inyeccion_on["state"] = "normal"
            btn_inyeccion_off["state"] = "disabled"
            pantalla.itemconfigure(inyeccion_label, text = "NO SE ESTÁ INYECTANDO GAS")
        else:
            pantalla.itemconfigure(inyeccion_label, text = "Algo falló al iniciar inyección de gas")
            
    except: 
        print("No hay comunicación con el sistema de inyección de flujo")


def log_data():
    global LOGGING
    LOGGING = True
    filename = "./datos_{}.csv".format(datetime.now())
    f = open(filename, "w")
    f.write("Tiempo en ns, Temperatura °C,Presion Torr,Flujo CH1 SCCM, Flujo CH2 SCCM\n")
    while LOGGING:
        f.write("{},{},{},{}\n".format(
            perf_counter_ns(),
            temperatura, 
            presion,
            flujo1,
            flujo2)
        )
        sleep(0.5)
    print("finalizar log de datos")
    f.close()



# crear gráficos
x,y  = [-x for x in range(200, 1, -1)], [25]*199
pressure_plot = grafico(pantalla, lambda: presion, (0,0), x,y, semilogy = True, ylim = (1e-5, 10e+2))
flow1_plot = grafico(pantalla, lambda: flujo1, (460,0), x,y, ylim = (0, 50))
flow2_plot = grafico(pantalla, lambda: flujo2, (920,0), x,y,ylim = (0, 50))

async def mediciones():
    global RUNNING
    while RUNNING:
        await aio.gather(
            t_temperatura(),
            t_presion(),
            t_flujo1(),
        )
    print("fin programa")


# las mediciones se deben correr en un hilo por aparte
def hilo_mediciones(): 
    aio.run(mediciones())

Thread(target = hilo_mediciones, args=()).start()


def hilo_encender_inyeccion():
    global MISC_TASKS
    MISC_TASKS.append(encender_inyeccion)

def hilo_apagar_inyeccion():
    global MISC_TASKS
    MISC_TASKS.append(apagar_inyeccion)

def hilo_gases_config_1():
    global MISC_TASKS
    MISC_TASKS.append(config_primera_inyeccion)

def hilo_gases_config_2():
    global MISC_TASKS
    MISC_TASKS.append(config_segunda_inyeccion)


def close():
    global RUNNING, LOGGING
    RUNNING = False
    LOGGING = False
    root.after(10, root.destroy)

root.protocol("WM_DELETE_WINDOW", close)

Button(root,width=20, text = "toggle Presión", command = lambda: pressure_plot.toggle(), font = ui_font).place(x = 40, y = 310)
Button(root,width=20, text = "toggle Flujo CH1", command = lambda: temp_plot.toggle(), font = ui_font).place(x = 540, y = 310)
Button(root,width=20, text = "toggle Flujo CH2", command = lambda: flow_plot.toggle(), font = ui_font).place(x = 1000, y = 310)
btn_inyeccion_on =  Button(root,height = 2,width=25, text = "inyectar gases", command = hilo_encender_inyeccion,font = ui_font)
btn_inyeccion_on.place(x = 950, y = 450)
btn_inyeccion_off = Button(root,heigh = 2, width=25, text = "detener inyección de gases", state = "disabled",command = hilo_apagar_inyeccion,font = ui_font)
btn_inyeccion_off.place(x = 950, y =550)
btn_gases_config1 = Button(root, text = "Configuración primera inyeccion",command = hilo_gases_config_1, font = ui_font)
btn_gases_config1.place(x = 20, y = 750)
btn_gases_config1 = Button(root, text = "Configuración segunda inyección",command = hilo_gases_config_2, font = ui_font)
btn_gases_config1.place(x = 520, y = 750)

def finalizar_reg_datos():
    global LOGGING, btn_log_on, btn_log_off
    btn_log_on["state"] = "normal"
    btn_log_off["state"] = "disabled"
    LOGGING = False

def empezar_reg_datos(): 
    global btn_log_on, btn_log_off
    btn_log_on["state"] = "disabled"
    btn_log_off["state"] = "normal"
    Thread(target = log_data, args = ()).start()

btn_log_on = Button(root,height=2, width = 27, text = "Empezar registro de datos", command = empezar_reg_datos, font = ui_font)
btn_log_on.place(x = 450, y = 450)
btn_log_off = Button(root,height=2, width = 27, text = "Finalizar registro de datos", state ="disabled" ,command = finalizar_reg_datos, font = ui_font)
btn_log_off.place(x = 450, y = 550)
root.mainloop()


