"""
Descripción del ejemplo:

Prende y apaga el led del sistema en intervalos de 1 segundo
"""

import control_host as ch
import asyncio as aio

###### Config ######

from serial.tools.list_ports import comports

puertos = comports()
mi_puerto = None

for puerto in puertos:
    if puerto.manufacturer == "PlasmaTEC" and puerto.product == "Control_Camara":
        mi_puerto = puerto.device

if mi_puerto == None:
    print("No se logró encontrar el microcontrolador")
    exit(1)

schema = ch.image('../firmware/target/thumbv7em-none-eabihf/release/sc-cfe')
port = ch.open_serial(schema, mi_puerto, 192000)

###### Programa ######

async def main():
    global port
    while (True):
        await port.led.blue()
        await aio.sleep(1)
        await port.led.off()
        await aio.sleep(1)

aio.run(main())





