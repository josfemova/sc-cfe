"""
Descripción del ejemplo:

Este ejemplo consiste de un programa que trata de obtener un valor de presión y otro de temperatura.
Termina al obtener ambos.
"""

import control_host as ch
import asyncio as aio

###### Config ######

from serial.tools.list_ports import comports

puertos = comports()
mi_puerto = None

for puerto in puertos:
    if puerto.manufacturer == "PlasmaTEC" and puerto.product == "Control_Camara":
        mi_puerto = puerto.device

if mi_puerto == None:
    print("No se logró encontrar el microcontrolador")
    exit(1)

schema = ch.image('../firmware/target/thumbv7em-none-eabihf/release/sc-cfe')
port = ch.open_serial(schema, mi_puerto, 192000)

###### Utilidades ######

def sci_a_str(x):
     sign = "+" if x.epositive else "-"
     return "{}.{:02d}E{}{}".format(x.integer, x.decimal, sign, x.exponent)

###### Programa ######

async def obtener_presion():
    global port #asegurarse de que port se refiere a la variable global
    x = await port.mks979b.pressure()
    if x!= None:
        presion = sci_a_str(x)
        print("presion es {} SCCM".format(presion))

async def obtener_temp():
    global port
    temp = await port.termocupla.temp()
    print("temp es {}".format(temp))

async def main():
    global port
    await port.led.green()
    await aio.gather(
            obtener_presion(),
            obtener_temp()
            )
    await port.led.red()
    print("fin")


aio.run(main())





