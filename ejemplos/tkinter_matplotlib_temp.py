"""
Descripción del ejemplo:

Aplicación GUI usando tkinter. Contiene un gráfico de matplotlib 
en el que se puede visualizar la temperatura actual.
"""
import control_host as ch
import asyncio as aio

from tkinter import *
from matplotlib.figure import Figure
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from threading import Thread

from datetime import datetime
from time import perf_counter_ns
from time import sleep

class grafico:
    """
    Clase para facilitar crear gráficos de matplotlib
    - root: variable que contiene la ventana de tkinter en que se quiere colocar el gráfico
    - poll_val: nombre de la función que se debe utilizar para obtener un nuevo dato del eje y
    - coords: cordenadas en donde se debe colocar el gráfico en la ventana
    - x: conjunto de datos iniciales para eje horizontal
    - y: conjunto de datos iniciales para eje vertical
    - size: dimensiones del gráfico de matplotlib
    - ylim: (menor valor de y esperado, mayor valor de y esperado)
    - wait_time: cada cuandos ms se actualiza el gráfico
    - semilogy: Poner o no el eje y en escala logarítmica
    """
    def __init__(self, root, poll_val,coords, x = [], y= [], size = (6,4), ylim = (10,70), wait_time=100, semilogy = False):
        self.x = x
        self.y = y
        self.i = 0
        self.fig = Figure(figsize=size, dpi = 100)
        self.ax = self.fig.add_subplot()
        if semilogy:
            self.line, = self.ax.semilogy(x,y)
        else :
            self.line, = self.ax.plot(x,y)
        self.ax.set_ylim(ylim)

        # canvas de tkinter
        self.canvas = FigureCanvasTkAgg(self.fig, master = root)
        self.canvas.draw()
        self.animation = FuncAnimation(self.fig, self.update, repeat = False,blit=False, interval=wait_time)
        self.poll_val = poll_val
        self.root = root
        self.canvas.get_tk_widget().place(x=coords[0], y=coords[1])
    
    def update(self,frame):
        self.x.append(self.i)
        self.i = self.i+1
        self.y.append(self.poll_val())
        self.x = self.x[-200:]
        self.y = self.y[-200:]
        self.line.set_data(self.x, self.y)
        self.ax.relim()
        self.ax.autoscale_view(True)
        return self.line,



# ventana de tkinter
root = Tk()
root.minsize(600,600)
root.resizable(width=NO, height=NO)
pantalla = Canvas(root, width=600, height=600, bg='white')
pantalla.place(x=0,y=0)
font = ("Envy Code R", 19)
temp_label = pantalla.create_text(300, 420, font = ("Envy Code R", 25))

###### Config ######

from serial.tools.list_ports import comports

puertos = comports()
mi_puerto = None

for puerto in puertos:
    if puerto.manufacturer == "PlasmaTEC" and puerto.product == "Control_Camara":
        mi_puerto = puerto.device

if mi_puerto == None:
    print("No se logró encontrar el microcontrolador")
    exit(1)

schema = ch.image('../firmware/target/thumbv7em-none-eabihf/release/sc-cfe')
port = ch.open_serial(schema, mi_puerto, 192000)


##### datos
temperatura = 25.0
RUNNING = True
LOGGING = True


async def t_temperatura():
    global port, temperatura,pantalla, temp_label, RUNNING
    while RUNNING:
        await aio.sleep(0)
        var = await port.termocupla.temp()
        temperatura = var/10.0
        pantalla.itemconfigure(temp_label, text = "T {} °C".format(temperatura))

async def main():
    await t_temperatura(),
    print("fin programa")

hilo = Thread(target = lambda: aio.run(main()))
hilo.start()


# crear gráfico
x,y  = [-x for x in range(200, 1, -1)], [25]*199
temp_plot = grafico(pantalla, lambda: temperatura, (0,0), x,y, ylim = (0, 50))

# administrar cuando cerramos la ventana
def close():
    global RUNNING, LOGGING
    RUNNING = False
    LOGGING = False
    root.after(50, root.destroy)

root.protocol("WM_DELETE_WINDOW", close)
root.mainloop()
