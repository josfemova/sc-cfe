#![no_std]
#![no_main]
//#![forbid(unsafe_code)]
#![feature(generic_associated_types)]

use itsybitsy_m4 as bsp;

use bsp::{
    hal::{
        self,
        adc::{Adc, FreeRunning, InterruptAdc},
        clock::GenericClockController,
        ehal::{
            timer::CountDown,
        },
        gpio::v2::*,
        pac::{self, gclk::pchctrl::GEN_A, ADC1},
        sercom::v2::{
            uart::{self, BaudMode, Oversampling},
            Sercom0, Sercom1,
        },
        time::MegaHertz,
        timer::TimerCounter,
        usb::UsbBus,
    },
    Hertz, IoSet3, IoSet3Sercom0Pad0, IoSet3Sercom0Pad2, Miso, Mosi, UndocIoSet2,
};

use smart_leds::{hsv::RGB8, SmartLedsWrite};
use usb_device::{bus::UsbBusAllocator, prelude::*};
use usbd_serial::{SerialPort, USB_CLASS_CDC};

use panic_halt as _;

use bitflags::bitflags;
use control_device::{actor, ControlDevice, DeviceMask, Monitor, Queues, Rpc};
use nb::Error::WouldBlock;
use nb_executor::{EventMask, Events};
use rtic::Mutex;
use serde::{Deserialize, Serialize};

use mks979b::{
    cmd::{Response, Setting, Value},
    fsm::{parse_response, Mks979b},
    scinumber::SciNumber979b,
};
use pr4000b::{data, param, Pr4000b};

bitflags! {
    pub struct Ev: u32 {
        const USB_RX = 1 << 0;
        const USB_TX = 1 << 1;
        const QUEUES = 1 << 2;
        const MKS979B_TX = 1 << 3;
        const MKS979B_RX = 1 << 4;
        const TEMP_READY = 1 << 5;
        const PR4000B_TX = 1 << 6;
        const PR4000B_RX = 1 << 7;
     }
}

impl EventMask for Ev {
    fn as_bits(self) -> u32 {
        self.bits()
    }
}

impl DeviceMask for Ev {
    fn monitor_tx() -> Self {
        Ev::USB_TX
    }
}

type Rgb = apa102_spi::Apa102<
    bitbang_hal::spi::SPI<
        Pin<PA27, Input<PullUp>>,
        Pin<PB03, Output<PushPull>>,
        Pin<PB02, Output<PushPull>>,
        hal::timer::TimerCounter<hal::pac::TC3>,
    >,
>;

#[derive(ControlDevice)]
#[events(Ev)]
pub struct Dev<'a> {
    monitor: Monitor<Dev<'a>>,
    led: LedActor<'a>,
    usb: app::UsbActor<'a>,
    mks979b: Mks979bActor<'a>,
    pr4000b: Pr4000bActor<'a>,
    termocupla: TempActor<'a>,
}

type Qu<'q, 'a> = &'q <Dev<'a> as ControlDevice>::Queues<'q>;

pub struct LedActor<'a> {
    rgb: &'a mut Rgb,
}

#[derive(Copy, Clone, Rpc, Serialize, Deserialize)]
pub struct Color {
    r: u8,
    g: u8,
    b: u8,
}

#[actor]
impl<'a> LedActor<'a> {
    #[rpc]
    async fn red(&mut self, _queues: Qu<'_, 'a>) {
        self.set(120, 0, 0);
    }

    #[rpc]
    async fn green(&mut self, _queues: Qu<'_, 'a>) {
        self.set(0, 120, 0);
    }

    #[rpc]
    async fn blue(&mut self, _queues: Qu<'_, 'a>) {
        self.set(0, 0, 120);
    }

    #[rpc]
    async fn off(&mut self, _queues: Qu<'_, 'a>) {
        self.set(0, 0, 0);
    }

    #[rpc]
    async fn rgb(&mut self, _queues: Qu<'_, 'a>, color: Color) -> Color {
        self.set(color.r, color.g, color.b);
        color
    }
}

impl LedActor<'_> {
    fn set(&mut self, r: u8, g: u8, b: u8) {
        let color = RGB8 { r, g, b };
        self.rgb.write(core::iter::once(color)).unwrap();
    }
}

pub struct TempActor<'a> {
    termocupla: &'a mut Termocupla,
}

#[actor]
impl<'a> TempActor<'a> {
    #[rpc]
    async fn temp(&mut self, queues: Qu<'_, 'a>) -> u32 {
        let sample = queues.signals().drive(Ev::TEMP_READY, || {
            let result = self.termocupla.temp().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::ADC1_RESRDY);
                }
                err
            });
            unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::ADC1_RESRDY);
            }
            result
        });
        if let Ok(t) = sample.await {
            ((t * 805861) - 1_250_000_000) / 5_000_000
        } else {
            0
        }
    }
}

pub struct Pr4000bActor<'a> {
    device: &'a mut Pr4000b<Uart1>,
}

#[derive(Copy, Clone, Rpc, Serialize, Deserialize)]
pub struct FlowValue {
    value: i32,
    decimal_point: u8,
}

impl From<data::Setpoint> for FlowValue {
    fn from(x: data::Setpoint) -> Self {
        FlowValue {
            value: x.value,
            decimal_point: x.decimal_point,
        }
    }
}

#[actor]
impl<'a> Pr4000bActor<'a> {
    #[rpc]
    async fn flow_ch1(&mut self, queues: Qu<'_, 'a>) -> Option<FlowValue> {
        let mut p = self
            .device
            .param(param::Actual)
            .channel(param::Channel1)
            .read();

        let value = queues.signals().drive(Ev::PR4000B_RX | Ev::PR4000B_TX, || {
            let result = p.poll().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
                }
                err
            });
            unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
            }
            result
        });

        if let Ok(setpoint) = value.await {
            Some(setpoint.into())
        } else {
            None
        }
    }
    #[rpc]
    async fn flow_ch2(&mut self, queues: Qu<'_, 'a>) -> Option<FlowValue> {
        let mut p = self
            .device
            .param(param::Actual)
            .channel(param::Channel2)
            .read();

        let value = queues.signals().drive(Ev::PR4000B_RX | Ev::PR4000B_TX, || {
            let result = p.poll().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
                }
                err
            });
            unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
            }
            result
        });

        if let Ok(setpoint) = value.await {
            Some(setpoint.into())
        } else {
            None
        }
    }
    #[rpc]
    async fn is_in_remote_mode(&mut self, queues: Qu<'_, 'a>) -> Option<bool> {
        let mut p = self.device.param(param::Remote).read();
        let value = queues.signals().drive(Ev::PR4000B_RX | Ev::PR4000B_TX, || {
            let result = p.poll().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
                }
                err
            });
            unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
            }
            result
        });
        if let Ok(a) = value.await {
            Some(a)
        } else {
            None
        }
    }
    #[rpc]
    async fn setpoint_ch1(&mut self, queues: Qu<'_, 'a>, setpoint: FlowValue) -> Option<FlowValue> {
        let sp = data::Setpoint{
            value: setpoint.value,
            decimal_point: setpoint.decimal_point
        };
        let mut p = self
            .device
            .param(param::Actual).channel(param::Channel1).set(sp);

        let value = queues.signals().drive(Ev::PR4000B_RX | Ev::PR4000B_TX, || {
            let result = p.poll().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
                }
                err
            });
            unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
            }
            result
        });

        if let Ok(result) = value.await {
            Some(result.into())
        } else {
            None
        }
    }
    #[rpc]
    async fn setpoint_ch2(&mut self, queues: Qu<'_, 'a>, setpoint: FlowValue) -> Option<FlowValue> {
        let value = data::Setpoint{
            value: setpoint.value,
            decimal_point: setpoint.decimal_point
        };
        let mut p = self
            .device
            .param(param::Actual).channel(param::Channel2).set(value);

        let value = queues.signals().drive(Ev::PR4000B_RX | Ev::PR4000B_TX, || {
            let result = p.poll().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
                }
                err
            });
            unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
            }
            result
        });

        if let Ok(result) = value.await {
            Some(result.into())
        } else {
            None
        }
    }
    #[rpc]
    async fn remote_mode(&mut self, queues: Qu<'_, 'a>, enable: bool) -> Option<bool> {
        let mut p = self.device.param(param::Remote).set(enable);
        let value = queues.signals().drive(Ev::PR4000B_RX | Ev::PR4000B_TX, || {
            let result = p.poll().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
                }
                err
            });
            unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
            }
            result
        });
        if let Ok(_) = value.await {
            Some(true)
        } else {
            None
        }
    }
    #[rpc]
    async fn valve_ch1(&mut self, queues: Qu<'_, 'a>, enable: bool) -> Option<bool> {
        let mut p = self
            .device
            .param(param::Valve)
            .channel(param::Channel1)
            .set(enable);
        let value = queues.signals().drive(Ev::PR4000B_RX | Ev::PR4000B_TX, || {
            let result = p.poll().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
                }
                err
            });
            unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
            }
            result
        });
        if let Ok(_) = value.await {
            Some(true)
        } else {
            None
        }
    }
    #[rpc]
    async fn valve_ch2(&mut self, queues: Qu<'_, 'a>, enable: bool) -> Option<bool> {
        let mut p = self
            .device
            .param(param::Valve)
            .channel(param::Channel2)
            .set(enable);
        let value = queues.signals().drive(Ev::PR4000B_RX | Ev::PR4000B_TX, || {
            let result = p.poll().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
                }
                err
            });
            unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
            }
            result
        });
        if let Ok(_) = value.await {
            Some(true)
        } else {
            None
        }
    }
    #[rpc]
    async fn gas_injection(&mut self, queues: Qu<'_, 'a>, turn_on: bool) -> Option<bool> {
        let mut p = self
            .device
            .param(param::Valve)
            .channel(param::Channel0)
            .set(turn_on);
        let value = queues.signals().drive(Ev::PR4000B_RX | Ev::PR4000B_TX, || {
            let result = p.poll().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
                }
                err
            });
            unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_1);
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM1_2);
            }
            result
        });
        if let Ok(_) = value.await {
            Some(true)
        } else {
            None
        }
    }
}

pub struct Mks979bActor<'a> {
    device: &'a mut Mks979b<bsp::Uart>,
}

#[derive(Copy, Clone, Rpc, Serialize, Deserialize)]
pub struct SciNumber {
    integer: u8,
    decimal: u8,
    epositive: bool,
    exponent: u8,
}

impl From<SciNumber979b> for SciNumber {
    fn from(x: SciNumber979b) -> Self {
        SciNumber {
            integer: x.integer,
            decimal: x.decimal,
            epositive: x.epositive,
            exponent: x.exponent,
        }
    }
}

#[actor]
impl<'a> Mks979bActor<'a> {
    #[rpc]
    async fn test_led(&mut self, queues: Qu<'_, 'a>, turn_on: bool) -> Option<bool> {
        let _ = self
            .device
            .schedule_message(Setting::TestLedON(turn_on).into());

        let tx = queues.signals().drive(Ev::MKS979B_TX, || {
            let result = self.device.send_message().map_err(|err| {
                unsafe { cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM3_1) };
                err
            });
            unsafe { cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM3_1) };
            result
        });

        let _ = tx.await;
        let rx = queues.signals().drive(Ev::MKS979B_RX, || {
            let result = self.device.poll().map_err(|err| {
                unsafe {
                    cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM3_2);
                }
                err
            });
            unsafe { cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM3_2) };
            result
        });

        if let Ok(Some(x)) = rx.await {
            if let Ok(Response::Boolean(val)) = parse_response(&x) {
                return Some(val);
            }
        }

        None
    }

    #[rpc]
    async fn pressure(&mut self, queues: Qu<'_, 'a>) -> Option<SciNumber> {
        let mut response = None;
        /*let mut response = Some(SciNumber {
            integer: 0,
            decimal: 0,
            epositive: true,
            exponent: 0,
        });*/
        let _ = self
            .device
            .schedule_message(Value::PressureReadingCombined.into());

        let tx = queues.signals().drive(Ev::MKS979B_TX, || {
            let result = self.device.send_message().map_err(|err| {
                unsafe { cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM3_1) };
                err
            });
            unsafe { cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM3_1) };
            result
        });

        let _ = tx.await;
        let rx = queues.signals().drive(Ev::MKS979B_RX, || {
            let result = self.device.poll().map_err(|err| unsafe {
                cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM3_2);
                err
            });
            unsafe { cortex_m::peripheral::NVIC::unmask(bsp::pac::interrupt::SERCOM3_2) };
            result
        });

        if let Ok(Some(x)) = rx.await {
            if let Ok(Response::SciNumber(val)) = parse_response(&x) {
                response = Some(val.into());
            }
        }

        response
    }
}

#[rtic::app(device = hal::pac)]
mod app {
    use super::*;

    pub struct UsbActor<'a> {
        usb: shared_resources::usb_that_needs_to_be_locked<'a>,
    }

    #[actor]
    impl<'a> UsbActor<'a> {
        async fn run(&mut self, queues: Qu<'_, 'a>) {
            let mut buf = heapless::Vec::new();

            loop {
                buf.resize_default(buf.capacity()).unwrap();
                let mut read = None;

                let ev = Ev::USB_RX | Ev::USB_TX;
                let io = queues.signals().drive_infallible(ev, || {
                    let tx = queues.monitor_tx();
                    let mut written = None;

                    self.usb.lock(|usb| {
                        read = usb.serial.read(&mut buf).ok();
                        if let Ok(mut tx) = tx {
                            written = usb.serial.write(tx.available()).ok();
                            if let Some(written) = written {
                                tx.consume(written);
                                queues.signals().pending().raise(Ev::USB_TX);
                            }
                        }

                        let _ = usb.serial.flush();
                    });

                    if read.is_some() || written.is_some() {
                        Ok(())
                    } else {
                        Err(WouldBlock)
                    }
                });

                io.await;

                if let Some(length) = read {
                    buf.truncate(length);
                    queues.monitor().feed(core::mem::take(&mut buf)).await;
                }
            }
        }
    }

    #[shared]
    struct Shared {
        usb: Usb,
        pending: Events<Ev>,
    }

    #[local]
    struct Local {
        rgb: apa102_spi::Apa102<
            bitbang_hal::spi::SPI<
                Pin<PA27, Input<PullUp>>,
                Pin<PB03, Output<PushPull>>,
                Pin<PB02, Output<PushPull>>,
                hal::timer::TimerCounter<hal::pac::TC3>,
            >,
        >,
        mks979b: Mks979b<bsp::Uart>,
        pr4000b: Pr4000b<Uart1>,
        //tc2: hal::timer::TimerCounter<hal::pac::TC2>,
        //led: Pin<PA22, PushPullOutput>,
        termocupla: Termocupla,
    }

    #[init(local = [
           usb_allocator: Option<UsbBusAllocator<UsbBus>> = None,
    ])]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        let mut peripherals = cx.device;
        let mut clocks = GenericClockController::with_internal_32kosc(
            peripherals.GCLK,
            &mut peripherals.MCLK,
            &mut peripherals.OSC32KCTRL,
            &mut peripherals.OSCCTRL,
            &mut peripherals.NVMCTRL,
        );

        let pins = bsp::Pins::new(peripherals.PORT);
        let gclk0 = clocks.gclk0();
        let tc2_3 = clocks.tc2_tc3(&gclk0).unwrap();
        let mut timer = TimerCounter::tc3_(&tc2_3, peripherals.TC3, &mut peripherals.MCLK);
        timer.start(MegaHertz(4));
        //let mut tc2 = TimerCounter::tc2_(&tc2_3, peripherals.TC2, &mut peripherals.MCLK);
        //tc2.start(bsp::Hertz(9600));
        //hal::timer_traits::InterruptDrivenTimer::enable_interrupt(&mut tc2);
        //let led = pins.d13.into_push_pull_output();
        let adc = Adc::<ADC1>::adc1(
            peripherals.ADC1,
            &mut peripherals.MCLK,
            &mut clocks,
            GEN_A::GCLK11,
        );
        let analog_pin = pins.a2;
        let mut termocupla = Termocupla::new(adc, analog_pin);
        termocupla.start_sampling();
        let bus_allocator = cx.local.usb_allocator.insert(bsp::usb_allocator(
            peripherals.USB,
            &mut clocks,
            &mut peripherals.MCLK,
            pins.usb_dm,
            pins.usb_dp,
        ));

        let serial = SerialPort::new(bus_allocator);
        let device = UsbDeviceBuilder::new(bus_allocator, UsbVidPid(0x16c0, 0x27dd))
            .manufacturer("PlasmaTEC")
            .product("Control_Camara")
            .serial_number("00000001")
            .device_class(USB_CLASS_CDC)
            .build();

        let mut serial3 = bsp::uart(
            &mut clocks,
            bsp::Hertz(9600),
            peripherals.SERCOM3,
            &mut peripherals.MCLK,
            pins.d0_rx,
            pins.d1_tx,
        );
        serial3.enable_interrupts(bsp::uart::Flags::RXC);
        serial3.enable_interrupts(bsp::uart::Flags::TXC);
        let mut serial1 = uart1(
            &mut clocks,
            bsp::Hertz(9600),
            peripherals.SERCOM1,
            &mut peripherals.MCLK,
            pins.miso,
            pins.mosi,
        );
        serial1.enable_interrupts(bsp::uart::Flags::RXC);
        serial1.enable_interrupts(bsp::uart::Flags::TXC);

        let mks979b = Mks979b::new(serial3, 253);
        let pr4000b = Pr4000b::new(serial1);
        let mut rgb = bsp::dotstar_bitbang(
            pins.dotstar_miso.into(),
            pins.dotstar_mosi.into(),
            pins.dotstar_sck.into(),
            timer,
        );

        rgb.write(
            [RGB8 {
                r: 120,
                g: 120,
                b: 120,
            }]
            .into_iter(),
        )
        .unwrap();

        (
            Shared {
                pending: Default::default(),
                usb: Usb { device, serial },
            },
            Local {
                rgb,
                mks979b,
                pr4000b,
                //tc2,
                //led,
                termocupla,
            },
            init::Monotonics(),
        )
    }

    #[idle(shared = [&pending, usb], local = [rgb, mks979b, pr4000b, termocupla])]
    fn idle(cx: idle::Context) -> ! {
        let signals = cx.shared.pending.watch();
        cx.local.termocupla.start_sampling();

        let mut dev = Dev {
            monitor: Monitor::default(),
            led: LedActor { rgb: cx.local.rgb },
            mks979b: Mks979bActor {
                device: cx.local.mks979b,
            },
            pr4000b: Pr4000bActor {
                device: cx.local.pr4000b,
            },
            usb: UsbActor { usb: cx.shared.usb },
            termocupla: TempActor {
                termocupla: cx.local.termocupla,
            },
        };

        let queues = Dev::queues(&signals, Ev::QUEUES);
        let future = async move {
            queues.usb().run().await;
            dev.run(&queues).await
        };

        signals.bind().block_on(future, |park| {
            cortex_m::interrupt::free(|_| {
                let parked = park.race_free();
                if parked.is_idle() {
                    rtic::export::wfi();
                }
                parked
            })
        })
    }

    #[task(binds = USB_OTHER, shared = [&pending, usb])]
    fn usb_other(cx: usb_other::Context) {
        usb_int(cx.shared.pending, cx.shared.usb);
    }

    #[task(binds = USB_TRCPT0, shared = [&pending, usb])]
    fn usb_trcpt0(cx: usb_trcpt0::Context) {
        usb_int(cx.shared.pending, cx.shared.usb);
    }

    #[task(binds = USB_TRCPT1, shared = [&pending, usb])]
    fn usb_trcpt1(cx: usb_trcpt1::Context) {
        usb_int(cx.shared.pending, cx.shared.usb);
    }

    #[task(binds = SERCOM3_1, shared=[&pending])]
    fn sercom3_1(cx: sercom3_1::Context) {
        cx.shared.pending.raise(Ev::MKS979B_TX);
        cortex_m::peripheral::NVIC::mask(bsp::pac::interrupt::SERCOM3_1);
    }
    #[task(binds = SERCOM3_2, shared=[&pending])]
    fn sercom3_2(cx: sercom3_2::Context) {
        cx.shared.pending.raise(Ev::MKS979B_RX);
        cortex_m::peripheral::NVIC::mask(bsp::pac::interrupt::SERCOM3_2);
    }
    #[task(binds = SERCOM1_1, shared=[&pending])]
    fn sercom1_1(cx: sercom1_1::Context) {
        cx.shared.pending.raise(Ev::PR4000B_TX);
        cortex_m::peripheral::NVIC::mask(bsp::pac::interrupt::SERCOM1_1);
    }
    #[task(binds = SERCOM1_2, shared=[&pending])]
    fn sercom1_2(cx: sercom1_2::Context) {
        cx.shared.pending.raise(Ev::PR4000B_RX);
        cortex_m::peripheral::NVIC::mask(bsp::pac::interrupt::SERCOM1_2);
    }

    #[task(binds= ADC1_RESRDY,shared=[&pending])]
    fn adc1_resrdy(cx: adc1_resrdy::Context) {
        cx.shared.pending.raise(Ev::TEMP_READY);
        cortex_m::peripheral::NVIC::mask(bsp::pac::interrupt::ADC1_RESRDY);
    }
}

fn usb_int(pending: &Events<Ev>, mut usb: impl Mutex<T = Usb>) {
    usb.lock(|usb| {
        if usb.device.poll(&mut [&mut usb.serial]) {
            let _ = usb.serial.read(&mut []);
            pending.raise(Ev::USB_RX);
        }
    });
}

pub struct Usb {
    device: UsbDevice<'static, UsbBus>,
    serial: SerialPort<'static, UsbBus>,
}

pub struct Termocupla {
    adc: InterruptAdc<ADC1, FreeRunning>,
    analog_pin: Pin<PB08, AlternateB>,
}

impl Termocupla {
    pub fn new(
        adc: impl Into<InterruptAdc<ADC1, FreeRunning>>,
        analog_pin: impl Into<Pin<PB08, AlternateB>>,
    ) -> Self {
        Termocupla {
            adc: adc.into(),
            analog_pin: analog_pin.into(),
        }
    }
    pub fn start_sampling(&mut self) {
        self.adc.start_conversion(&mut self.analog_pin);
    }
    pub fn temp(&mut self) -> nb::Result<u32, nb::Error<Self>> {
        if let Some(x) = self.adc.service_interrupt_ready() {
            Ok(x as u32)
        } else {
            Err(nb::Error::WouldBlock)
        }
    }
}

pub type UartPads0 = uart::Pads<Sercom0, IoSet3, IoSet3Sercom0Pad2, IoSet3Sercom0Pad0>;
pub type Uart0 = uart::Uart<uart::Config<UartPads0>, uart::Duplex>;

/// Utility function for setting up SERCOM0 pins as an additional
/// UART peripheral.
pub fn uart0(
    clocks: &mut GenericClockController,
    baud: impl Into<Hertz>,
    sercom0: pac::SERCOM0,
    mclk: &mut pac::MCLK,
    uart_rx: impl Into<IoSet3Sercom0Pad2>,
    uart_tx: impl Into<IoSet3Sercom0Pad0>,
) -> Uart0 {
    let gclk0 = clocks.gclk0();
    let clock = &clocks.sercom0_core(&gclk0).unwrap();
    let baud = baud.into();
    let pads = uart::Pads::default().rx(uart_rx.into()).tx(uart_tx.into());
    let config = uart::Config::new(mclk, sercom0, pads, clock.freq())
        .baud(baud, BaudMode::Fractional(Oversampling::Bits16));

    unsafe {
        (*pac::SERCOM0::ptr())
            .usart_int_mut()
            .ctrla
            .modify(|_, w| w.txpo().bits(0));
    };
    config.enable()
}

pub type UartPads1 = uart::Pads<Sercom1, UndocIoSet2, Miso, Mosi>;
pub type Uart1 = uart::Uart<uart::Config<UartPads1>, uart::Duplex>;

/// Utility function for setting up SERCOM0 pins as an additional
/// UART peripheral.
pub fn uart1(
    clocks: &mut GenericClockController,
    baud: impl Into<Hertz>,
    sercom1: pac::SERCOM1,
    mclk: &mut pac::MCLK,
    uart_rx: impl Into<Miso>,
    uart_tx: impl Into<Mosi>,
) -> Uart1 {
    let gclk0 = clocks.gclk0();
    let clock = &clocks.sercom1_core(&gclk0).unwrap();
    let baud = baud.into();
    let pads = uart::Pads::default().rx(uart_rx.into()).tx(uart_tx.into());
    //Configure UART for use with the PR4000B with Odd parity
    let config = uart::Config::new(mclk, sercom1, pads, clock.freq())
        //.parity(Parity::Odd)
        .baud(baud, BaudMode::Fractional(Oversampling::Bits16));
    unsafe {
        (*pac::SERCOM1::ptr())
            .usart_int_mut()
            .ctrla
            .modify(|_, w| w.txpo().bits(0));
    };
    config.enable()
}
